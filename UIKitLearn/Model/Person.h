//
//  Person.h
//  UIKitLearn
//
//  Created by 李旭阳[产品技术中心] on 2020/6/3.
//  Copyright © 2020 李旭阳[产品技术中心]. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface Person : NSObject<NSCoding>


//属性
@property(nonatomic, copy)NSString *name;//姓名
@property(nonatomic, copy)NSString *city;//城市
@property(nonatomic, assign)NSInteger age;//年龄


//方法
- (void)work;//工作
- (void)sleep;//睡觉

@end


