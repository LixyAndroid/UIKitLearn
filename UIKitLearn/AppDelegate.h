//
//  AppDelegate.h
//  UIKitLearn
//
//  Created by 李旭阳[产品技术中心] on 2020/5/25.
//  Copyright © 2020 李旭阳[产品技术中心]. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>


@end

