//
//  SaveDataController.m
//  UIKitLearn
//
//  Created by 李旭阳[产品技术中心] on 2020/6/3.
//  Copyright © 2020 李旭阳[产品技术中心]. All rights reserved.
//

#define username @"username"
#define userpassword @"userpassword"
#define path  NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject

#import "SaveDataController.h"
#import "Person.h"
#import "SQLiteDataBase.h"


@interface SaveDataController ()

@property(nonatomic, strong)SQLiteDataBase *personDB;

@property(nonatomic, strong)UITextField *nameField;
@property(nonatomic, strong)UITextField *cityField;
@property(nonatomic, strong)UITextField *ageField;
@end

@implementation SaveDataController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"%@",path);
    // Do any additional setup after loading the view.
    
       
       //初始化数据库
    self.personDB = [[SQLiteDataBase alloc] initWithType:DataBaseWithNative];
    [self setupUI];
}

#pragma mark -- UI布局
//UI布局
- (void)setupUI{
    
    
   NSArray *titles = @[@"偏好设置",@"plist",@"归档解归档"];
    
    for (int i = 0; i<titles.count; i++) {
        
        UIButton* button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [button setTitle:titles[i] forState:UIControlStateNormal];

        button.frame = CGRectMake(20+i*120, 100, 100, 100);
        button.tag = 50+i;
        [button addTarget:self action:@selector(savaDataWithType:) forControlEvents:UIControlEventTouchDown];

        [self.view addSubview:button];
    }
    
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.frame = CGRectMake(self.view.frame.size.width/2-50, 250, 100, 100);
    [titleLabel setText:@"SQLite"];
    titleLabel.font = [UIFont systemFontOfSize:28.0f];
    
    [self.view addSubview:titleLabel];
    //数据库
    
    NSArray *dataBaseTitles = @[@"增",@"删",@"改",@"查",@"打开",@"关闭"];

    
    
    for (int i = 0; i<dataBaseTitles.count; i++) {
        
        UIButton* button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [button setTitle:dataBaseTitles[i] forState:UIControlStateNormal];

        button.frame = CGRectMake(10+i*60, 400, 100, 100);
        button.tag = 60+i;
        [button addTarget:self action:@selector(buttonWithSQLite:) forControlEvents:UIControlEventTouchDown];

        [self.view addSubview:button];
    }
    
    [self creatTextField];
    
    
}


//创建person属性输入框
- (void )creatTextField{
    
    
    NSMutableArray * array = [NSMutableArray arrayWithCapacity:3];
    NSArray *titles = @[@"姓名",@"城市",@"年龄"];
    for (int i = 0; i<titles.count; i++) {
        UITextField *textfield = [[UITextField alloc]initWithFrame:CGRectMake(50, 500+i*100, self.view.frame.size.width*0.6, 50)];;
        textfield.placeholder = titles[i];
        textfield.borderStyle = UITextBorderStyleRoundedRect;
        textfield.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:textfield];
        [array addObject:textfield];
    }
   
    self.nameField = array[0];
    self.cityField = array[1];
    self.ageField = array[2];
}

#pragma mark -- 按钮方法
- (void)savaDataWithType:(UIButton *)button{
    switch (button.tag) {
        case 50:
            [self dataWithUserDefaults];
            break;
        case 51:
            [self dataWithPlist];
            break;
        case 52:
            [self dataWithKeyedArchiver];
            break;
        default:
            break;
    }
}

#pragma mark -- NSUserDefaults
//偏好设置
//和Android中 SharedPreferences  一样，设置key,value
- (void)dataWithUserDefaults{
    //获取NSUserDefaults文件
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    //保存内容
    [userDefaults setValue:@"zhangsan" forKey:username];
    [userDefaults setValue:@"abc123" forKey:userpassword];
    
    //读取内容
    NSString *name = [userDefaults valueForKey:userpassword];
    NSString *password = [userDefaults valueForKey:userpassword];
    NSLog(@"username:%@----userpassword:%@",name,password);
    
}

#pragma mark --plist存储
//plist文件存储
- (void)dataWithPlist{
    NSArray *array = @[@{@"name":@"林五",@"sex":@"男"},@{@"name":@"苏三",@"sex":@"女"}];
    //设置文件名
    NSString *fileName = [path stringByAppendingPathComponent:@"students.plist"];
    //写入文件
    [array writeToFile:fileName atomically:YES];
    //读取
    NSArray *result = [NSArray arrayWithContentsOfFile:fileName];
    
    //输出中文有问题
    NSLog(@"students.plist ---%@",result);
}

- (void)dataWithKeyedArchiver{
    
    Person *per = [[Person alloc] init];
    per.name = @"王明";
    per.age = 26;
    per.city = @"广州";
    
    
    //设置文件名
    NSString *fileName = [path stringByAppendingPathComponent:@"person.archiver"];
    //进行归档
    [NSKeyedArchiver archiveRootObject:per toFile:fileName];
    //进行解归档
    Person *person = [NSKeyedUnarchiver unarchiveObjectWithFile:fileName];
    
    NSLog(@"Person----name:%@,age:%ld,city:%@",person.name,person.age,person.city);
}



#pragma mark -- 数据库按钮方法

- (void)buttonWithSQLite:(UIButton *)button{
    switch (button.tag) {
        case 60:
            [self SQLiteWithAdd];
            break;
        case 61:
            [self SQLiteWithDelete];
            break;
        case 62:
            [self SQLiteWithUpdate];
            break;
        case 63:
            [self SQLiteWithSelect];
            break;
        case 64:
            [self SQLiteWithOpen];
            break;
        case 65:
            [self SQLiteWithClose];
            break;
        default:
            break;
    }
}

#pragma mark -- SQLite
- (void)SQLiteWithAdd{
    NSLog(@"asdasdas");
    Person *per = [Person new];
    per.name = self.nameField.text;
    per.age = self.ageField.text.integerValue;
    per.city = self.cityField.text;
    [self.personDB createTable];
    [self.personDB addPerson:per];
}
- (void)SQLiteWithDelete{
    Person *per = [Person new];
    per.name = self.nameField.text;
    per.age = self.ageField.text.integerValue;
    per.city = self.cityField.text;
    [self.personDB deletePerson:per];
}

- (void)SQLiteWithUpdate{
    Person *per = [Person new];
    per.name = self.nameField.text;
    per.age = self.ageField.text.integerValue;
    per.city = self.cityField.text;
    [self.personDB updatePerson:per];
}

- (void)SQLiteWithSelect{
    NSMutableArray *array = [self.personDB selectAllPerson];
    if (array.count != 0) {
        for (Person *per in array) {
            NSLog(@"Person----name:%@,age:%ld,city:%@",per.name,per.age,per.city);
        }
    }else{
        NSLog(@"没有查到数据");
    }
    
}

- (void)SQLiteWithOpen{
    [self.personDB open];
}

- (void)SQLiteWithClose{
    [self.personDB close];
}






@end
