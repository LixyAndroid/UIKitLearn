//
//  WebViewViewController.m
//  UIKitLearn
//
//  Created by 李旭阳[产品技术中心] on 2020/5/25.
//  Copyright © 2020 李旭阳[产品技术中心]. All rights reserved.
//

#import "WebViewViewController.h"
#import "WKWebViewController.h"


@interface WebViewViewController ()


@end

@implementation WebViewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupUI];
}


- (void)setupUI {
    
    self.title = @"WKWebView";
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIButton *wkBasicUseButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [wkBasicUseButton setTitle:@"WKWebView基础使用" forState:UIControlStateNormal];
    wkBasicUseButton.backgroundColor = [UIColor grayColor];
    
    //创建label对象，并指定位置，self.view.frame.size.width/2表示横中心点，self.view.frame.size.height/2表示垂直中心点
    UILabel *lable0 = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-50, self.view.frame.size.height*0.6, 200, 100)];
            
    //label 的内容
    lable0.text = @"界面跳转的学习";
            
    //文字的颜色
    lable0.textColor = [UIColor redColor];
     [self.view addSubview:lable0];
    
    [self.view addSubview:wkBasicUseButton];
    [wkBasicUseButton sizeToFit];
    wkBasicUseButton.center = self.view.center;
    [wkBasicUseButton addTarget:self action:@selector(wkBasicUseButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
   
}

- (void)wkBasicUseButtonClicked:(UIButton *)sender {
   
   [self.navigationController pushViewController:[WKWebViewController new] animated:YES];
}


@end
