//
//  CustomToolbarViewController.m
//  UIKitLearn
//
//  Created by 李旭阳[产品技术中心] on 2020/5/28.
//  Copyright © 2020 李旭阳[产品技术中心]. All rights reserved.
//

#import "CustomToolbarViewController.h"

@interface CustomToolbarViewController ()
@property (nonatomic, weak) IBOutlet UIToolbar *toolbar;

@end

@implementation CustomToolbarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self configureToolbar];
}
#pragma mark - Configuration

- (void)configureToolbar {
    UIImage *toolbarBackgroundImage = [UIImage imageNamed:@"toolbar_background"];
    //背景图片
    [self.toolbar setBackgroundImage:toolbarBackgroundImage forToolbarPosition:UIBarPositionBottom barMetrics:UIBarMetricsDefault];

    NSArray *toolbarButtonItems = @[[self customImageBarButtonItem], [self flexibleSpaceBarButtonItem], [self customBarButtonItem]];
    [self.toolbar setItems:toolbarButtonItems animated:YES];
}


#pragma mark - UIBarButtonItem Creation and Configuration

- (UIBarButtonItem *)customImageBarButtonItem {
    UIBarButtonItem *customImageBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"tools_icon"] style:UIBarButtonItemStylePlain target:self action:@selector(barButtonItemClicked:)];

    customImageBarButtonItem.tintColor = [UIColor purpleColor];

    return customImageBarButtonItem;
}

- (UIBarButtonItem *)flexibleSpaceBarButtonItem {
    return [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:NULL];
}

- (UIBarButtonItem *)customBarButtonItem {
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Button", nil) style:UIBarButtonItemStylePlain target:self action:@selector(barButtonItemClicked:)];

    NSDictionary *attributes = @{NSForegroundColorAttributeName: [UIColor purpleColor]};
    [barButtonItem setTitleTextAttributes:attributes forState:UIControlStateNormal];

    return barButtonItem;
}


#pragma mark - Actions

- (void)barButtonItemClicked:(UIBarButtonItem *)barButtonItem {
    NSLog(@"A bar button item on the custom toolbar was clicked: %@.", barButtonItem);
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
