//
//  TableViewController.m
//  UIKitLearn
//
//  Created by 李旭阳[产品技术中心] on 2020/5/28.
//  Copyright © 2020 李旭阳[产品技术中心]. All rights reserved.
//

#import "TableViewController.h"


@interface TableViewController ()<UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource>

@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) UIView *navigationBar;
@property (weak, nonatomic) NSLayoutConstraint *navigationBarTopConstraint;
@property (assign, nonatomic, readonly) CGFloat navigationBarHeight;
@property (strong, nonatomic) UIView *searchBar;
@property (weak, nonatomic)  NSLayoutConstraint *searchBarTopConstraint;
@property (assign, nonatomic, readonly) CGFloat searchBarHeight;
@property (assign, nonatomic) CGFloat previousOffsetY;


@end

@implementation TableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = UIColor.whiteColor;

    [self _addSubViewBase];
    
    CGFloat insetsTop = self.navigationBarHeight + self.searchBarHeight;
    UIEdgeInsets insets = UIEdgeInsetsMake(insetsTop, 0, 0, 0);
    self.tableView.contentInset = insets;
    self.tableView.contentOffset = CGPointMake(0, -insetsTop);
}

- (void)_addSubViewBase{
    
    //ios11适配
    if (@available(iOS 11.0,*)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }else{
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    [self.view addSubview:self.tableView];
    if (@available(iOS 11.0,*)) {
        [[self.tableView.topAnchor constraintEqualToAnchor:self.view.safeAreaLayoutGuide.topAnchor] setActive:YES];
        
        [[self.tableView.bottomAnchor constraintEqualToAnchor:self.view.safeAreaLayoutGuide.bottomAnchor] setActive:YES];
    }else{
        [[self.tableView.topAnchor constraintEqualToAnchor:self.topLayoutGuide.bottomAnchor] setActive:YES];
        [[self.tableView.bottomAnchor constraintEqualToAnchor:self.bottomLayoutGuide.topAnchor] setActive:YES];
    }
    [[self.tableView.leftAnchor constraintEqualToAnchor:self.view.leftAnchor] setActive:YES];
    [[self.tableView.rightAnchor constraintEqualToAnchor:self.view.rightAnchor] setActive:YES];
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 150)];
    headerView.backgroundColor = UIColor.purpleColor;
    self.tableView.tableHeaderView = headerView;
    
    // 最后加入导航条，显示在最上方
    [self.tableView addSubview:self.searchBar];
    [self.tableView addSubview:self.navigationBar];
    
    NSLayoutConstraint *searchBarTopConstraint = [self.searchBar.topAnchor constraintEqualToAnchor:self.navigationBar.bottomAnchor];
    [searchBarTopConstraint setActive:YES];
    self.searchBarTopConstraint = searchBarTopConstraint;
    [[self.searchBar.leftAnchor constraintEqualToAnchor:self.tableView.leftAnchor] setActive:YES];
    [[self.searchBar.rightAnchor constraintEqualToAnchor:self.tableView.rightAnchor] setActive:YES];
    [[self.searchBar.widthAnchor constraintEqualToAnchor:self.tableView.widthAnchor] setActive:YES];
    [[self.searchBar.heightAnchor constraintEqualToConstant:self.searchBarHeight] setActive:YES];
    
    NSLayoutConstraint *navigationBarTopConstraint = [self.navigationBar.topAnchor constraintEqualToAnchor:self.tableView.topAnchor];
    [navigationBarTopConstraint setActive:YES];
    self.navigationBarTopConstraint = navigationBarTopConstraint;
    [[self.navigationBar.leftAnchor constraintEqualToAnchor:self.tableView.leftAnchor] setActive:YES];
    [[self.navigationBar.rightAnchor constraintEqualToAnchor:self.tableView.rightAnchor] setActive:YES];
    [[self.navigationBar.widthAnchor constraintEqualToAnchor:self.tableView.widthAnchor] setActive:YES];
    [[self.navigationBar.heightAnchor constraintEqualToConstant:self.navigationBarHeight] setActive:YES];
    
}


#pragma mark - UITableViewDelegate, UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 26;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"cell"];
    cell.textLabel.text = [NSString stringWithFormat:@"%@-%@", @(indexPath.section), @(indexPath.row)];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", @(indexPath.section)];

    return cell;
}

//点击事件，重要的是 要页面跳转，以及信息传递
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  
    
    NSString *str = [NSString stringWithFormat:@"%@-%@", @(indexPath.section), @(indexPath.row)];

    
    NSLog(@"%@-%@", @(indexPath.section), @(indexPath.row));
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @(section).stringValue;
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat offsetY = scrollView.contentOffset.y;
    CGFloat topHeight = self.navigationBarHeight + self.searchBarHeight;
    
    self.navigationBarTopConstraint.constant = MAX(offsetY, -topHeight);
    
    CGFloat height = CGRectGetHeight(scrollView.frame);
    CGFloat contentHeight = scrollView.contentSize.height;
    if (offsetY > -topHeight &&
        offsetY < contentHeight - height) {
        CGFloat translationY = offsetY - self.previousOffsetY;
        CGFloat maxTop = 0;
        CGFloat nextTop = self.searchBarTopConstraint.constant - translationY;
        CGFloat minTop = -self.searchBarHeight;
        CGFloat top = MIN(MAX(minTop, nextTop), maxTop);
        self.searchBarTopConstraint.constant = top;
        
        // 解决有分组存在时，区头偏移问题
        self.tableView.contentInset = UIEdgeInsetsMake(topHeight + top, 0, 0, 0);
        
        // 这句话不能放在括号外，否则会出现临界条件时，搜索框瞬间消失的现象
        self.previousOffsetY = offsetY;
    }
    
    // 解决有弹性时，搜索框偏移异常
    if (offsetY <= -topHeight) {
        self.tableView.contentInset = UIEdgeInsetsMake(topHeight, 0, 0, 0);
    }
}

#pragma mark - get method
#pragma mark -

- (UITableView *)tableView
{
    if (_tableView == nil) {
        UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.backgroundColor = UIColor.blueColor;
        tableView.tableFooterView = UIView.new;
        tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
        tableView.showsVerticalScrollIndicator = NO;
        tableView.showsHorizontalScrollIndicator = NO;
        tableView.translatesAutoresizingMaskIntoConstraints = NO;
        tableView.bounces = YES;
        tableView.alwaysBounceVertical = YES;
        _tableView = tableView;
    }
    return _tableView;
}

- (UIView *)navigationBar
{
    if (_navigationBar == nil) {
        UIView *navigationBar = [[UIView alloc] init];
        navigationBar.backgroundColor = UIColor.redColor;
        navigationBar.translatesAutoresizingMaskIntoConstraints = NO;
        navigationBar.layer.zPosition = 3;
        _navigationBar = navigationBar;
    }
    return _navigationBar;
}

- (CGFloat)navigationBarHeight
{
    return 44;
}

- (UIView *)searchBar
{
    if (_searchBar == nil) {
        UIView *searchBar = [[UIView alloc] init];
        searchBar.backgroundColor = UIColor.greenColor;
        searchBar.translatesAutoresizingMaskIntoConstraints = NO;
        searchBar.layer.zPosition = 2;
        _searchBar = searchBar;
    }
    return _searchBar;
}

- (CGFloat)searchBarHeight
{
    return 56;
}



@end
