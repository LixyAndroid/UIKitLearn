//
//  SlidersViewController.m
//  UIKitLearn
//
//  Created by 李旭阳[产品技术中心] on 2020/5/28.
//  Copyright © 2020 李旭阳[产品技术中心]. All rights reserved.
//

#import "SlidersViewController.h"

@interface SlidersViewController ()


@property (nonatomic, weak) IBOutlet UISlider *defaultSlider;
@property (nonatomic, weak) IBOutlet UISlider *tintedSlider;
@property (nonatomic, weak) IBOutlet UISlider *customSlider;
@end

@implementation SlidersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureDefaultSlider];
    [self configureTintedSlider];
    [self configureCustomSlider];

}

#pragma mark - Configuration

- (void)configureDefaultSlider {
    self.defaultSlider.minimumValue = 0;
    self.defaultSlider.maximumValue = 100;
    self.defaultSlider.value = 42;
    self.defaultSlider.continuous = YES;
    
    [self.defaultSlider addTarget:self action:@selector(sliderValueDidChange:) forControlEvents:UIControlEventValueChanged];
}

- (void)configureTintedSlider {
    self.tintedSlider.minimumTrackTintColor = [UIColor blueColor];
    self.tintedSlider.maximumTrackTintColor = [UIColor purpleColor];

    [self.tintedSlider addTarget:self action:@selector(sliderValueDidChange:) forControlEvents:UIControlEventValueChanged];
}

- (void)configureCustomSlider {
    
    //图片
    UIImage *leftTrackImage = [UIImage imageNamed:@"slider_blue_track"];
    [self.customSlider setMinimumTrackImage:leftTrackImage forState:UIControlStateNormal];
    
    UIImage *rightTrackImage = [UIImage imageNamed:@"slider_green_track"];
    [self.customSlider setMaximumTrackImage:rightTrackImage forState:UIControlStateNormal];
    
    UIImage *thumbImage = [UIImage imageNamed:@"slider_thumb"];
    [self.customSlider setThumbImage:thumbImage forState:UIControlStateNormal];
    
    self.customSlider.minimumValue = 0;
    self.customSlider.maximumValue = 100;
    self.customSlider.continuous = NO;
    self.customSlider.value = 84;

    [self.customSlider addTarget:self action:@selector(sliderValueDidChange:) forControlEvents:UIControlEventValueChanged];
}


#pragma mark - Actions

- (void)sliderValueDidChange:(UISlider *)slider {
    NSLog(@"A slider changed its value: %@", slider);
}


@end
