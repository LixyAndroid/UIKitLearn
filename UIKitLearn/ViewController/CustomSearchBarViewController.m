//
//  CustomSearchBarViewController.m
//  UIKitLearn
//
//  Created by 李旭阳[产品技术中心] on 2020/5/28.
//  Copyright © 2020 李旭阳[产品技术中心]. All rights reserved.
//

#import "CustomSearchBarViewController.h"

@interface CustomSearchBarViewController ()
@property (nonatomic, weak) IBOutlet UISearchBar *searchBar;

@end

@implementation CustomSearchBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //viewDidLoad 的时候执行configureSearchBar方法
    [self configureSearchBar];
}

#pragma mark - Configuration

- (void)configureSearchBar {
    self.searchBar.showsCancelButton = YES;
    self.searchBar.showsBookmarkButton = YES;
    
    self.searchBar.tintColor = [UIColor purpleColor];
    
    self.searchBar.backgroundImage = [UIImage imageNamed:@"search_bar_background"];

    // Set the bookmark image for both normal and highlighted states.
    UIImage *bookmarkImage = [UIImage imageNamed:@"bookmark_icon"];
    [self.searchBar setImage:bookmarkImage forSearchBarIcon:UISearchBarIconBookmark state:UIControlStateNormal];

    UIImage *bookmarkHighlightedImage = [UIImage imageNamed:@"bookmark_icon_highlighted"];
    [self.searchBar setImage:bookmarkHighlightedImage forSearchBarIcon:UISearchBarIconBookmark state:UIControlStateHighlighted];
}

#pragma mark - UISearchBarDelegate

//点击键盘的搜索按钮时会来到该方法
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSLog(@"The custom search bar keyboard search button was tapped: %@.", searchBar.text);

    [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    NSLog(@"The custom search bar cancel button was tapped.");

    [searchBar resignFirstResponder];
}

- (void)searchBarBookmarkButtonClicked:(UISearchBar *)searchBar {
    NSLog(@"The custom bookmark button inside the search bar was tapped.");
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
