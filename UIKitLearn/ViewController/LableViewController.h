//
//  LableViewController.h
//  UIKitLearn
//
//  Created by 李旭阳[产品技术中心] on 2020/5/26.
//  Copyright © 2020 李旭阳[产品技术中心]. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LableViewController : UIViewController{}

//声明一个label控件；这样就拿到了一个全局的label
@property (nonatomic,strong) UILabel *lable;
@end

NS_ASSUME_NONNULL_END
