//
//  LableViewController.m
//  UIKitLearn
//
//  Created by 李旭阳[产品技术中心] on 2020/5/26.
//  Copyright © 2020 李旭阳[产品技术中心]. All rights reserved.
//

#import "LableViewController.h"

@interface LableViewController ()

@end

@implementation LableViewController




- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setUpView];

}
//一定要关联storyboard的scene!!!
/*
 UIViewAutoresizingFlexibleLeftMargin 自动调整与superView左边的距离，保证与superView右边的距离不变。

 UIViewAutoresizingFlexibleRightMargin 自动调整与superView的右边距离，保证与superView左边的距离不变。

 UIViewAutoresizingFlexibleTopMargin 自动调整与superView顶部的距离，保证与superView底部的距离不变。

 UIViewAutoresizingFlexibleBottomMargin 自动调整与superView底部的距离，也就是说，与superView顶部的距离不变。

 UIViewAutoresizingFlexibleWidth 自动调整自己的宽度，保证与superView左边和右边的距离不变。

 UIViewAutoresizingFlexibleHeight 自动调整自己的高度，保证与superView顶部和底部的距离不变。

 UIViewAutoresizingFlexibleLeftMargin |UIViewAutoresizingFlexibleRightMargin 自动调整与superView左边的距离，保证与左边的距离和右边的距离和原来距左边和右边的距离的比例不变。比如原来距离为20，30，调整后的距离应为68，102，即68/20=102/30。
 */

- (void) setUpView{
    
    //创建label对象，并指定位置，self.view.frame.size.width/2表示横中心点，self.view.frame.size.height/2表示垂直中心点
    UILabel *lable0 = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-100, self.view.frame.size.height*0.8, 200, 200)];
        
        //label 的内容
        lable0.text = @"Hello World! my name is xuyang.li. my age is 25!";
        
        //文字的颜色
        lable0.textColor = [UIColor redColor];
             
    
        //clearColor 是指透明颜色,表示label对象的背景颜色，包括整个位置
        lable0.backgroundColor =[UIColor clearColor];
        
     
        
        //label的高级属性
        //设置字体大小
        lable0.font =[UIFont systemFontOfSize:18];
        
        //label 的阴影的颜色
        lable0.shadowColor = [UIColor grayColor];
        //label 阴影的偏移量
       lable0.shadowOffset = CGSizeMake(2, -5);
        
        //居中对齐
        lable0.textAlignment = NSTextAlignmentCenter;
        
        //如果文字超过显示  使用 0 则是自动调整行数
        lable0.numberOfLines = 0;
    
    //创建label对象，并指定位置，self.view.frame.size.width/2表示横中心点，self.view.frame.size.height/2表示垂直中心点
    self.lable = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-100, self.view.frame.size.height/2-100, 200, 200)];
     
     //label 的内容
     self.lable.text = @"Hello World! my name is xuyang.li. my age is 25!";
     
     //文字的颜色
     self.lable.textColor = [UIColor redColor];
          
 
     //clearColor 是指透明颜色,表示label对象的背景颜色，包括整个位置
     self.lable.backgroundColor =[UIColor clearColor];
     
     //背景颜色
     self.view.backgroundColor = [UIColor cyanColor];
     
     //label的高级属性
     //设置字体大小
     self.lable.font =[UIFont systemFontOfSize:18];
     
     //label 的阴影的颜色
     self.lable.shadowColor = [UIColor grayColor];
     //label 阴影的偏移量
     self.lable.shadowOffset = CGSizeMake(2, -5);
     
     //居中对齐
     self.lable.textAlignment = NSTextAlignmentCenter;
     
     //如果文字超过显示  使用 0 则是自动调整行数
     self.lable.numberOfLines = 0;
    
    //button
    
    UIButton* button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    button.frame = CGRectMake(self.view.frame.size.width/2-50, self.view.frame.size.height/3, 100, 100);
    
    // 按钮的正常状态,forState表示状态
    [button setTitle:@"点击" forState:UIControlStateNormal];


    // 按钮的按下状态
    [button setTitle:@"按下" forState:UIControlStateHighlighted];

    // 设置按钮的背景色
    button.backgroundColor = [UIColor redColor];

    // 设置正常状态下按钮文字的颜色，如果不写其他状态，默认都是用这个文字的颜色
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

    // 设置按下状态文字的颜色
    [button setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];

    // 设置按钮的风格颜色,只有titleColor没有设置的时候才有用
    [button setTintColor:[UIColor whiteColor]];

    // titleLabel：UILabel控件
    button.titleLabel.font = [UIFont systemFontOfSize:25];
    
    
    // Button监听事件，非常重要
    /**
     *addTarget:目标（让谁做这个事情）
     *action:方法（做什么事情-->方法）
     *forControlEvents:事件  在什么时候执行
     */
    
    [button addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];

    
     
     // 添加到视图
    [self.view addSubview:lable0];
     [self.view addSubview:self.lable];
     [self.view addSubview:button];
    

}



// 对应触发函数如下

// 参数为调用此函数按钮对象本身
-(void)btnClick: (UIButton *) button {
    NSLog(@"%@",button);
    //设置不能再点击
    button.enabled = NO;
    self.lable.font = [UIFont systemFontOfSize:29];
    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
