//
//  SwitchsTableViewController.m
//  UIKitLearn
//
//  Created by 李旭阳[产品技术中心] on 2020/5/28.
//  Copyright © 2020 李旭阳[产品技术中心]. All rights reserved.
//

#import "SwitchsTableViewController.h"

@interface SwitchsTableViewController ()
@property (nonatomic, weak) IBOutlet UISwitch *defaultSwitch;
@property (nonatomic, weak) IBOutlet UISwitch *tintedSwitch;
@end

@implementation SwitchsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureDefaultSwitch];
    [self configureTintedSwitch];
}

#pragma mark - Configuration

- (void)configureDefaultSwitch {
    [self.defaultSwitch setOn:YES animated:YES];

    [self.defaultSwitch addTarget:self action:@selector(switchValueDidChange:) forControlEvents:UIControlEventValueChanged];
}

- (void)configureTintedSwitch {
    
    //设置颜色
    self.tintedSwitch.tintColor = [UIColor blueColor];
    self.tintedSwitch.onTintColor = [UIColor greenColor];
    //按钮
    self.tintedSwitch.thumbTintColor = [UIColor purpleColor];

    [self.tintedSwitch addTarget:self action:@selector(switchValueDidChange:) forControlEvents:UIControlEventValueChanged];
}


#pragma mark - Actions

- (void)switchValueDidChange:(UISwitch *)aSwitch {
    NSLog(@"A switch changed its value: %@.", aSwitch);
}

@end
