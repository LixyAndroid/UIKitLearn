//
//  DefaultSearchBarViewController.m
//  UIKitLearn
//
//  Created by 李旭阳[产品技术中心] on 2020/5/28.
//  Copyright © 2020 李旭阳[产品技术中心]. All rights reserved.
//

#import "DefaultSearchBarViewController.h"

@interface DefaultSearchBarViewController ()

//属性
@property (nonatomic, weak) IBOutlet UISearchBar *searchBar;

@end

@implementation DefaultSearchBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   [self configureSearchBar];

}



#pragma mark - Configuration

- (void)configureSearchBar {
    self.searchBar.showsCancelButton = YES;
    self.searchBar.showsScopeBar = YES;

    self.searchBar.scopeButtonTitles = @[
        NSLocalizedString(@"Scope One", nil),
        NSLocalizedString(@"Scope Two", nil)
    ];
}


#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope {
    NSLog(@"The default search selected scope button index changed to %ld.", (long)selectedScope);
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSLog(@"The default search bar keyboard search button was tapped: %@.", searchBar.text);
    
    [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    NSLog(@"The default search bar cancel button was tapped.");
    
    [searchBar resignFirstResponder];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
