//
//  ButtonsViewController.h
//  UIKitLearn
//
//  Created by 李旭阳[产品技术中心] on 2020/5/25.
//  Copyright © 2020 李旭阳[产品技术中心]. All rights reserved.
//


//导入UIKit包
#import <UIKit/UIKit.h>


//宏
/*
 在这两个宏之间的代码，所有简单指针都被假定设为非空(nonnull),因此我们只需要去指定那些可为空的(nullable)的指针，这样不用麻烦的去将每个属性或方法都去指定nonnull和nullable，减轻了开发的工作量。
 */
NS_ASSUME_NONNULL_BEGIN

@interface ButtonsViewController :UITableViewController

@end

NS_ASSUME_NONNULL_END
