//
//  PickerViewController.m
//  UIKitLearn
//
//  Created by 李旭阳[产品技术中心] on 2020/5/25.
//  Copyright © 2020 李旭阳[产品技术中心]. All rights reserved.
//

#import "PickerViewController.h"

typedef NS_ENUM(NSInteger, PickerViewControllerColorComponent) {
    ColorComponentRed = 0,
    ColorComponentGreen,
    ColorComponentBlue,
    ColorComponentCount
};

// The maximum RGB color
#define RGB_MAX 255.0



@interface PickerViewController ()<UIPickerViewDataSource, UIPickerViewDelegate, UIPickerViewAccessibilityDelegate>

@property (nonatomic, weak) IBOutlet UIPickerView *pickerView;
@property (nonatomic, weak) IBOutlet UIView *colorSwatchView;

@property (nonatomic) CGFloat redColorComponent;
@property (nonatomic) CGFloat greenColorComponent;
@property (nonatomic) CGFloat blueColorComponent;

@end

@implementation PickerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    // Show that a given row is selected. This is off by default.
    
    //显示选择指示器
    //self.pickerView.showsSelectionIndicator = YES;

    [self configurePickerView];
}
// The offset of each color value (from 0 to 255) for red, green, and blue.

//步进
- (NSInteger)colorValueOffset {
    return 1;
}

- (NSInteger)numberOfColorValuesPerComponent {
    
    //设置值的范围
    return (NSInteger)ceil(RGB_MAX / (CGFloat)[self colorValueOffset]) + 1;
}

- (void)updateColorSwatchViewBackgroundColor {
    self.colorSwatchView.backgroundColor = [UIColor colorWithRed:self.redColorComponent green:self.greenColorComponent blue:self.blueColorComponent alpha:1];
}


#pragma mark - Configuration

//初始值
- (void)configurePickerView {
    // Set the default selected rows (the desired rows to initially select will vary by use case).
    // 设置默认选择的行(初始选择的所需行因用例而异)。
    [self selectRowInPickerView:10 withColorComponent:ColorComponentRed];
    [self selectRowInPickerView:15 withColorComponent:ColorComponentGreen];
    [self selectRowInPickerView:20 withColorComponent:ColorComponentBlue];
}

- (void)selectRowInPickerView:(NSInteger)row withColorComponent:(PickerViewControllerColorComponent)colorComponent {
    // Note that the delegate method on UIPickerViewDelegate is not triggered when manually calling -[UIPickerView selectRow:inComponent:animated:].
    // To do this, we fire off the delegate method manually.
    [self.pickerView selectRow:row inComponent:(NSInteger)colorComponent animated:YES];
    [self pickerView:self.pickerView didSelectRow:row inComponent:(NSInteger)colorComponent];
}


#pragma mark - RGB Color Setter Overrides

- (void)setRedColorComponent:(CGFloat)redColorComponent {
    if (_redColorComponent != redColorComponent) {
        _redColorComponent = redColorComponent;

        [self updateColorSwatchViewBackgroundColor];
    }
}

- (void)setGreenColorComponent:(CGFloat)greenColorComponent {
    if (_greenColorComponent != greenColorComponent) {
        _greenColorComponent = greenColorComponent;

        [self updateColorSwatchViewBackgroundColor];
    }
}

- (void)setBlueColorComponent:(CGFloat)blueColorComponent {
    if (_blueColorComponent != blueColorComponent) {
        _blueColorComponent = blueColorComponent;

        [self updateColorSwatchViewBackgroundColor];
    }
}


#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return ColorComponentCount;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [self numberOfColorValuesPerComponent];
}


#pragma mark - UIPickerViewDelegate

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSInteger colorValue = row * [self colorValueOffset];

    CGFloat colorComponent = (CGFloat)colorValue / RGB_MAX;
    CGFloat redColorComponent = 0;
    CGFloat greenColorComponent = 0;
    CGFloat blueColorComponent = 0;
    NSLog(@"%d",component);
    switch (component) {
        case ColorComponentRed:
            redColorComponent = colorComponent;
           
            break;
        case ColorComponentGreen:
            greenColorComponent = colorComponent;
            break;
        case ColorComponentBlue:
            blueColorComponent = colorComponent;
            break;
        default:
            NSLog(@"Invalid row/component combination for picker view.");
            break;
    }

    UIColor *foregroundColor = [UIColor colorWithRed:redColorComponent green:greenColorComponent blue:blueColorComponent alpha:1];

    NSString *titleText = [NSString stringWithFormat:@"%ld", (long)colorValue];

    // Set the foreground color for the attributed string.
    NSDictionary *attributes = @{NSForegroundColorAttributeName: foregroundColor};
    NSAttributedString *title = [[NSAttributedString alloc] initWithString:titleText attributes:attributes];

    return title;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    CGFloat colorComponentValue = ((CGFloat)[self colorValueOffset] * row)/RGB_MAX;

    switch (component) {
        case ColorComponentRed:
            self.redColorComponent = colorComponentValue;
            break;

        case ColorComponentGreen:
            self.greenColorComponent = colorComponentValue;
            break;

        case ColorComponentBlue:
            self.blueColorComponent = colorComponentValue;
            break;
            
        default:
            NSLog(@"Invalid row/component combination selected for picker view.");
            break;
    }
}


#pragma mark - UIPickerViewAccessibilityDelegate

- (NSString *)pickerView:(UIPickerView *)pickerView accessibilityLabelForComponent:(NSInteger)component {
    NSString *accessibilityLabel;

    switch (component) {
        case ColorComponentRed:
            accessibilityLabel = NSLocalizedString(@"Red color component value", nil);
            break;
            
        case ColorComponentGreen:
            accessibilityLabel = NSLocalizedString(@"Green color component value", nil);
            break;
            
        case ColorComponentBlue:
            accessibilityLabel = NSLocalizedString(@"Blue color component value", nil);
            break;
            
        default:
            NSLog(@"Invalid row/component combination for picker view.");
            break;
    }

    return accessibilityLabel;
}




@end
