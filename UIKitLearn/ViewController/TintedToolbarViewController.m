//
//  TintedToolbarViewController.m
//  UIKitLearn
//
//  Created by 李旭阳[产品技术中心] on 2020/5/28.
//  Copyright © 2020 李旭阳[产品技术中心]. All rights reserved.
//

#import "TintedToolbarViewController.h"

@interface TintedToolbarViewController ()
@property (nonatomic, weak) IBOutlet UIToolbar *toolbar;

@end

@implementation TintedToolbarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self configureToolbar];
}

#pragma mark - Configuration

- (void)configureToolbar {
    // See the UIBarStyle enum for more styles, including UIBarStyleDefault.
    self.toolbar.barStyle = UIBarStyleBlackTranslucent;

    self.toolbar.tintColor = [UIColor greenColor];
    self.toolbar.backgroundColor = [UIColor blueColor];
    
    NSArray *toolbarButtonItems = @[[self refreshBarButtonItem], [self flexibleSpaceBarButtonItem], [self actionBarButtonItem]];
    [self.toolbar setItems:toolbarButtonItems animated:YES];
}


#pragma mark - UIBarButtonItem Creation and Configuration

- (UIBarButtonItem *)refreshBarButtonItem {
    return [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(barButtonItemClicked:)];
}

- (UIBarButtonItem *)flexibleSpaceBarButtonItem {
    return [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:NULL];
}

- (UIBarButtonItem *)actionBarButtonItem {
    return [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(barButtonItemClicked:)];
}


#pragma mark - Actions

- (void)barButtonItemClicked:(UIBarButtonItem *)barButtonItem {
    NSLog(@"A bar button item on the tinted toolbar was clicked: %@.", barButtonItem);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
