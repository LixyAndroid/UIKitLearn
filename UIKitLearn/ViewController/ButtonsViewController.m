//
//  ButtonsViewController.m
//  UIKitLearn
//
//  Created by 李旭阳[产品技术中心] on 2020/5/25.
//  Copyright © 2020 李旭阳[产品技术中心]. All rights reserved.
//

#import "ButtonsViewController.h"

#import "UIView+Toast.h"

@interface ButtonsViewController()




@property (nonatomic, weak) IBOutlet UIButton *systemTextButton;
@property (nonatomic, weak) IBOutlet UIButton *systemContactAddButton;
@property (nonatomic, weak) IBOutlet UIButton *systemDetailDisclosureButton;
@property (nonatomic, weak) IBOutlet UIButton *imageButton;
@property (nonatomic, weak) IBOutlet UIButton *attributedTextButton;

@end

@implementation ButtonsViewController

//计数
int cnt = 0;

- (void)viewDidLoad {
    [super viewDidLoad];

    // All of the buttons are created in the storyboard, but configured below.
    [self configureSystemTextButton];
    [self configureSystemContactAddButton];
    [self configureSystemDetailDisclosureButton];
    [self configureImageButton];
    [self configureAttributedTextSystemButton];
}

#pragma mark - Configuration

- (void)configureSystemTextButton {
    [self.systemTextButton setTitle:NSLocalizedString(@"Button", nil) forState:UIControlStateNormal];

    [self.systemTextButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)configureSystemContactAddButton {
    self.systemContactAddButton.backgroundColor = [UIColor clearColor];

    [self.systemContactAddButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)configureSystemDetailDisclosureButton {
    self.systemDetailDisclosureButton.backgroundColor = [UIColor clearColor];

    [self.systemDetailDisclosureButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)configureImageButton {
    // To create this button in code you can use +[UIButton buttonWithType:] with a parameter value of UIButtonTypeCustom.

    // Remove the title text.
    [self.imageButton setTitle:@"" forState:UIControlStateNormal];

    self.imageButton.tintColor = [UIColor orangeColor];

    [self.imageButton setImage:[UIImage imageNamed:@"x_icon"] forState:UIControlStateNormal];

    // Add an accessibility label to the image.
    self.imageButton.accessibilityLabel = NSLocalizedString(@"X Button", nil);

    [self.imageButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)configureAttributedTextSystemButton {
    NSDictionary *titleAttributes = @{NSForegroundColorAttributeName: [UIColor blueColor], NSStrikethroughStyleAttributeName: @(NSUnderlineStyleSingle)};
    NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Button", nil) attributes:titleAttributes];
    [self.attributedTextButton setAttributedTitle:attributedTitle forState:UIControlStateNormal];

    NSDictionary *highlightedTitleAttributes = @{NSForegroundColorAttributeName : [UIColor greenColor], NSStrikethroughStyleAttributeName: @(NSUnderlineStyleThick)};
    NSAttributedString *highlightedAttributedTitle = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Button", nil) attributes:highlightedTitleAttributes];
    [self.attributedTextButton setAttributedTitle:highlightedAttributedTitle forState:UIControlStateHighlighted];

    [self.attributedTextButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - Actions


// Handler for all of AAPLButtonViewController's UIButton actions.
- (void)buttonClicked:(UIButton *)button {
    NSLog(@"A button was clicked: %@.", button);
    
}
- (IBAction)systemButton:(UIButton *)sender {
    
        cnt++;
       NSString *cntSting = [NSString stringWithFormat:@"%d",cnt];
        NSLog(cntSting);
    [self.view makeToast:cntSting
                duration:2.0
                position:CSToastPositionTop];
}

@end
