//
//  AuthorViewController.m
//  UIKitLearn
//
//  Created by 李旭阳[产品技术中心] on 2020/5/28.
//  Copyright © 2020 李旭阳[产品技术中心]. All rights reserved.
//

#import "AuthorViewController.h"

@interface AuthorViewController ()

@end

@implementation AuthorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    
        UIImage *image = [UIImage imageNamed:@"xuyang"];
        UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
        [imageView setFrame:CGRectMake(0, 0, image.size.width, image.size.height)];
        //居中
        imageView.center = self.view.center;
    
        imageView.layer.cornerRadius = 10;
        imageView.clipsToBounds = YES;
         [self.view addSubview:imageView];
    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
