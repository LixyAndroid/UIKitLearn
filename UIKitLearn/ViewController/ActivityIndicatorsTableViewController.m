//
//  ActivityIndicatorsTableViewController.m
//  UIKitLearn
//
//  Created by 李旭阳[产品技术中心] on 2020/5/27.
//  Copyright © 2020 李旭阳[产品技术中心]. All rights reserved.
//

#import "ActivityIndicatorsTableViewController.h"

@interface ActivityIndicatorsTableViewController ()


@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *grayStyleActivityIndicatorView;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *tintedActivityIndicatorView;

@end



@implementation ActivityIndicatorsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self configerGrayActivityIndicatorView];
    [self configureTintedActivityIndicatorView];
}



- (void)configerGrayActivityIndicatorView{
    
    self.grayStyleActivityIndicatorView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleLarge;
    

    [self.grayStyleActivityIndicatorView startAnimating];
    

    self.grayStyleActivityIndicatorView.hidesWhenStopped = YES;
}

- (void)configureTintedActivityIndicatorView {
    self.tintedActivityIndicatorView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleMedium;

    self.tintedActivityIndicatorView.color = [UIColor purpleColor];

    [self.tintedActivityIndicatorView startAnimating];
}


- (IBAction)stopAnimatingGray:(id)sender {
    [self.grayStyleActivityIndicatorView stopAnimating];

}


@end
