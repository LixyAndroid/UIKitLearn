//
//  AlertTableViewController.m
//  UIKitLearn
//
//  Created by 李旭阳[产品技术中心] on 2020/5/27.
//  Copyright © 2020 李旭阳[产品技术中心]. All rights reserved.
//

#import "AlertTableViewController.h"

// Corresponds to the row in the action sheet section.
typedef NS_ENUM(NSInteger, AlertsViewControllerTableRow) {
    AlertsViewControllerAlertViewRowSimple = 0,
    AlertsViewControllerAlertViewRowOkayCancel,
    AlertsViewControllerAlertViewRowOther,
    AlertsViewControllerAlertViewRowTextEntry,
    AlertsViewControllerActionSheetRowTextEntrySecure
};
@interface AlertTableViewController ()
@property (nonatomic, strong) NSArray *alertArrayData;

@end

@implementation AlertTableViewController


// Show an alert with an "Okay" button.
- (void)showSimpleAlert {
NSString *title = NSLocalizedString(@"A Short Title Is Best", nil);
NSString *message = NSLocalizedString(@"A message should be a short, complete sentence.", nil);
NSString *cancelButtonTitle = NSLocalizedString(@"OK", nil);

 UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
  //增加确定按钮；
    [alertController addAction:[UIAlertAction actionWithTitle:cancelButtonTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action){
        
    }]];

    [self presentViewController:alertController animated:true completion:nil];
}

// Show an alert with an "Okay" and "Cancel" button.
- (void)showOkayCancelAlert {
    NSString *title = NSLocalizedString(@"A Short Title Is Best", nil);
    NSString *message = NSLocalizedString(@"A message should be a short, complete sentence.", nil);
    NSString *cancelButtonTitle = NSLocalizedString(@"Cancel", nil);
    NSString *otherButtonTitle = NSLocalizedString(@"OK", nil);
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
           //增加取消按钮；
       [alertController addAction:[UIAlertAction actionWithTitle:cancelButtonTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}]];//增加确定按钮；
       [alertController addAction:[UIAlertAction actionWithTitle:otherButtonTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action){}]];

       [self presentViewController:alertController animated:true completion:nil];

}

// Show an alert with two custom buttons.
- (void)showOtherAlert {
    NSString *title = NSLocalizedString(@"A Short Title Is Best", nil);
    NSString *message = NSLocalizedString(@"A message should be a short, complete sentence.", nil);
    NSString *cancelButtonTitle = NSLocalizedString(@"Cancel", nil);
    NSString *otherButtonTitleOne = NSLocalizedString(@"Choice One", nil);
    NSString *otherButtonTitleTwo = NSLocalizedString(@"Choice Two", nil);

    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
              //增加取消按钮；
          [alertController addAction:[UIAlertAction actionWithTitle:cancelButtonTitle style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}]];

        [alertController addAction:[UIAlertAction actionWithTitle:otherButtonTitleOne style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action){}]];
    
     [alertController addAction:[UIAlertAction actionWithTitle:otherButtonTitleTwo style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action){}]];

          [self presentViewController:alertController animated:true completion:nil];
}

// Show a text entry alert with two custom buttons.
- (void)showTextEntryAlert {
    NSString *title = NSLocalizedString(@"A Short Title Is Best", nil);
    NSString *message = NSLocalizedString(@"A message should be a short, complete sentence.", nil);
    NSString *cancelButtonTitle = NSLocalizedString(@"Cancel", nil);
    NSString *otherButtonTitle = NSLocalizedString(@"OK", nil);
    
 
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
        //增加取消按钮；
    [alertController addAction:[UIAlertAction actionWithTitle:cancelButtonTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {}]];//增加确定按钮；
    [alertController addAction:[UIAlertAction actionWithTitle:otherButtonTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action){}]];
    
     [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
         textField.placeholder = nil;
     }];

    [self presentViewController:alertController animated:true completion:nil];
}

// Show a secure text entry alert with two custom buttons.
- (void)showSecureTextEntryAlert {
    NSString *title = NSLocalizedString(@"A Short Title Is Best", nil);
    NSString *message = NSLocalizedString(@"A message should be a short, complete sentence.", nil);
    NSString *cancelButtonTitle = NSLocalizedString(@"Cancel", nil);
    NSString *otherButtonTitle = NSLocalizedString(@"OK", nil);

    
  //  1、创建UIAlertController

    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];

  //  2、添加一个输入框 添加输入框文字改变监听

    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField){

            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(alertTextFieldDidChange:) name:UITextFieldTextDidChangeNotification object:textField];

            textField.placeholder = @"Password";

        }];

    //3、在确定和取消按钮事件中相应的移除监听

    UIAlertAction *cancleAction = [UIAlertAction actionWithTitle:cancelButtonTitle style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {

        }];

        UIAlertAction *okAction = [UIAlertAction actionWithTitle:otherButtonTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {

            UITextField * wordNameTextField = alertController.textFields.firstObject;//获取到textField

            //监听
            [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:nil];

        }];

        okAction.enabled = NO;//此处确定按钮在输入框内没有内容时置灰不可用

        //添加
        [alertController addAction:cancleAction];

        [alertController addAction:okAction];

        [self presentViewController:alertController animated:YES completion:nil];
}



- (void)alertTextFieldDidChange:(NSNotification *)notification{

    UIAlertController *alertController = (UIAlertController *)self.presentedViewController;

    if (alertController) {

        UITextField *wordNameTextField = alertController.textFields.firstObject;

        UIAlertAction *okAction = alertController.actions.lastObject;

        okAction.enabled = wordNameTextField.text.length>5;//输入框有内容时可用

    }

}





- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    _alertArrayData = [NSArray arrayWithObjects:@"Simple",@"Okey/Cancel",@"Other",@"Text Entry",@"Secure Text Entry",nil];

}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _alertArrayData.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *indentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:indentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:indentifier];
    }

    cell.textLabel.text = [_alertArrayData objectAtIndex:indexPath.row];
  
    return cell;
}





#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    AlertsViewControllerTableRow row = indexPath.row;

    switch (row) {
           case AlertsViewControllerAlertViewRowSimple:
               [self showSimpleAlert];
               break;
           case AlertsViewControllerAlertViewRowOkayCancel:
               [self showOkayCancelAlert];
               break;
           case AlertsViewControllerAlertViewRowOther:
               [self showOtherAlert];
               break;
           case AlertsViewControllerAlertViewRowTextEntry:
               [self showTextEntryAlert];
               break;
           case AlertsViewControllerActionSheetRowTextEntrySecure:
               [self showSecureTextEntryAlert];
               break;
           default:
               break;
       }

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}



@end
