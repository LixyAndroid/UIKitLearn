//
//  ActionSheetTableViewController.m
//  UIKitLearn
//
//  Created by 李旭阳[产品技术中心] on 2020/5/26.
//  Copyright © 2020 李旭阳[产品技术中心]. All rights reserved.
//

#import "ActionSheetTableViewController.h"
// Corresponds to the row in the action sheet section.
typedef NS_ENUM(NSInteger, ActionSheetsViewControllerTableRow) {
    AlertsViewControllerActionSheetRowOkayCancel = 0,
    AlertsViewControllerActionSheetRowOther
};

@interface ActionSheetTableViewController ()
@property (nonatomic, strong) NSArray *arrayData;

@end

@implementation ActionSheetTableViewController

// Show a dialog with an "Okay" and "Cancel" button.
- (void)showOkayCancelActionSheet {
    NSString *cancelButtonTitle = @"Cancel";
    NSString *destructiveButtonTitle = @"OK";
    

    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];

    //block是将函数及其上下文封装起来的对象。
    [actionSheet addAction:[UIAlertAction actionWithTitle:destructiveButtonTitle style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        NSLog(destructiveButtonTitle);

    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:cancelButtonTitle style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
           NSLog(cancelButtonTitle);
           
       }]];
    

    [self presentViewController:actionSheet animated:true completion:nil];
}

// Show a dialog with two custom buttons.
- (void)showOtherActionSheet {
    NSString *destructiveButtonTitle = @"Destructive Choice";
    NSString *otherButtonTitle = @"Safe Choice";

    
     UIAlertController *actionSheetController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];

    [actionSheetController addAction:[UIAlertAction actionWithTitle:otherButtonTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
         NSLog(otherButtonTitle);

     }]];
    [actionSheetController addAction:[UIAlertAction actionWithTitle:destructiveButtonTitle style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        NSLog(destructiveButtonTitle);
        
    }]];

   [self presentViewController:actionSheetController animated:true completion:nil];
    
}



@synthesize arrayData;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    arrayData = [NSArray arrayWithObjects:@"Okay/Cancel",@"Other", nil];

}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    //倍数显示，一般为1
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrayData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *indentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:indentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:indentifier];
    }

    cell.textLabel.text = [arrayData objectAtIndex:indexPath.row];
  
    return cell;
}
#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ActionSheetsViewControllerTableRow row = indexPath.row;

    switch (row) {
        case AlertsViewControllerActionSheetRowOkayCancel:
            NSLog(@"hahahah");
            [self showOkayCancelActionSheet];
            break;
        case AlertsViewControllerActionSheetRowOther:
            [self showOtherActionSheet];
            break;
        default:
            break;
    }

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
