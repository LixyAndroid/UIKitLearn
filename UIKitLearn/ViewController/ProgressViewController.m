//
//  ProgressViewController.m
//  UIKitLearn
//
//  Created by 李旭阳[产品技术中心] on 2020/5/27.
//  Copyright © 2020 李旭阳[产品技术中心]. All rights reserved.
//

#import "ProgressViewController.h"

const NSUInteger kProgressViewControllerMaxProgress = 100;

@interface ProgressViewController ()

@property (nonatomic, weak) IBOutlet UIProgressView *defaultStyleProgressView;
@property (nonatomic, weak) IBOutlet UIProgressView *barStyleProgressView;
@property (nonatomic, weak) IBOutlet UIProgressView *tintedProgressView;

@property (nonatomic) NSOperationQueue *operationQueue;
@property (nonatomic) NSUInteger completedProgress;
@end

@implementation ProgressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // All progress views should initially have zero progress.
       self.completedProgress = 0;

       [self configureDefaultStyleProgressView];
       [self configureBarStyleProgressView];
       [self configureTintedProgressView];

       // As progress is received from another subsystem (i.e. NSProgress, NSURLSessionTaskDelegate, etc.), update the progressView's progress.
       [self simulateProgress];
    
}


// Overrides the "completedProgress" property's setter.
- (void)setCompletedProgress:(NSUInteger)completedProgress {
    if (_completedProgress != completedProgress) {
        float fractionalProgress = (float)completedProgress / (float)kProgressViewControllerMaxProgress;

        [self.defaultStyleProgressView setProgress:fractionalProgress animated:YES];

        [self.barStyleProgressView setProgress:fractionalProgress animated:YES];

        [self.tintedProgressView setProgress:fractionalProgress animated:YES];

        _completedProgress = completedProgress;
    }
}


#pragma mark - Configuration

- (void)configureDefaultStyleProgressView {
    self.defaultStyleProgressView.progressViewStyle = UIProgressViewStyleDefault;
}

- (void)configureBarStyleProgressView {
    self.barStyleProgressView.progressViewStyle = UIProgressViewStyleBar;
}

- (void)configureTintedProgressView {
    self.tintedProgressView.progressViewStyle = UIProgressViewStyleDefault;

    self.tintedProgressView.trackTintColor = [UIColor blueColor];
    self.tintedProgressView.progressTintColor = [UIColor purpleColor];
}


#pragma mark - Progress Simulation

- (void)simulateProgress {
    // In this example we will simulate progress with a "sleep operation".
    self.operationQueue = [[NSOperationQueue alloc] init];
    
    for (NSUInteger count = 0; count < kProgressViewControllerMaxProgress; count++) {
        [self.operationQueue addOperationWithBlock:^{
            // Delay the system for a random number of seconds.
            // This code is _not_ intended for production purposes. The "sleep" call is meant to simulate work done in another subsystem.
            
            //将系统延迟随机秒数。
            //此代码不用于生产目的。“sleep”调用用于模拟在另一个子系统中完成的工作。
            sleep(arc4random_uniform(20));
            
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                self.completedProgress++;
            }];
        }];
    }
}




@end
